import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { WebstartComponent } from './webstart.component';
import { WebstartRoutingModule } from './webstart-routing.module';
import { SystemModule } from '../system/system.module';

@NgModule({
  declarations: [WebstartComponent],
  imports: [BrowserModule, WebstartRoutingModule, SystemModule],
  providers: [],
  bootstrap: [WebstartComponent],
})
export class WebstartModule {}
